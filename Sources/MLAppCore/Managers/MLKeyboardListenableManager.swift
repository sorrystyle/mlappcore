//
//  MLKeyboardListenableManager.swift
//  
//
//  Created by Marc Liu on 2022/10/7.
//

import Foundation
import UIKit

public protocol MLKeyboardListenableManagerDelegate {
    func keyboardWillShow(notification: NSNotification)
    func keyboardDidShown(notification: NSNotification)
    func keyboardWillDismiss(notification: NSNotification)
}


public class MLKeyboardListenableManager: NSObject {
    public var delegates = MLMultiDelegate<MLKeyboardListenableManagerDelegate>()
    
    override init() {
        super.init()
        addListener()
    }

    deinit {
        removeListener()
    }
    
    private func addListener() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDismiss(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShown(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
    }
    
    private func removeListener() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc
    func keyboardWillShow(notification: NSNotification) {
        delegates.invoke({ $0.keyboardWillShow(notification: notification)})
    }

    @objc
    func keyboardWillDismiss(notification: NSNotification) {
        delegates.invoke({ $0.keyboardWillDismiss(notification: notification) })
    }
    
    @objc
    func keyboardDidShown(notification: NSNotification) {
        delegates.invoke({ $0.keyboardDidShown(notification: notification) })
    }
}
