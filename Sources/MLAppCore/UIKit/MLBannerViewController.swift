//
//  MLBannerViewController.swift
//  Neko Republic
//
//  Created by Marc Liu on 2022/10/3.
//

import UIKit
import Kingfisher

public struct BannerInfo: Equatable {
    public var bannerView: UIView
    public var userInfo: Any?
    
    public init(bannerView: UIView, userInfo: Any) {
        self.bannerView = bannerView
        self.userInfo = userInfo
    }
    
    public static func == (lhs: BannerInfo, rhs: BannerInfo) -> Bool {
        return lhs.bannerView == rhs.bannerView
    }
}

public struct BannerPresentable {
    public var banners: [BannerInfo]
    public var firstPlaceHolder: BannerInfo
    public var lastPlaceHolder: BannerInfo
    
    public init(banners: [BannerInfo], firstPlaceHolder: BannerInfo, lastPlaceHolder: BannerInfo) {
        self.banners = banners
        self.firstPlaceHolder = firstPlaceHolder
        self.lastPlaceHolder = lastPlaceHolder
    }
}

public protocol BannerViewControllerDelegate: AnyObject {
    func bannerInfo() -> BannerPresentable?
    func bannerWidth() -> CGFloat
    func bannderDidTapped(info: BannerInfo)
    func placeHolderImage() -> UIImage
    func hideIndicatorLabel() -> Bool
    func pageControl() -> UIPageControl?
    func enableTimer() -> Bool
}

public class BannerViewController: UIViewController {
    private let bannerSwitchInterval = 3.0
    
    private var scv = UIScrollView()
    private var indicatorLabel = UILabel()
    private var pageControl: UIPageControl?
    
    private var stackView: UIStackView?
    
    public weak var delegate: BannerViewControllerDelegate?
    
    private var bannersToDisplay = [BannerInfo]()
    
    private var banners = [BannerInfo]()

    private var timer: Timer?
    
    private var pageIndex: Int = 1 {
        didSet {
            var currentPage = pageIndex - 1
            if pageIndex == 0 {
                currentPage = bannersToDisplay.count - 2
            }
            if pageIndex == (bannersToDisplay.count - 1) {
                currentPage = 0
            }
            indicatorLabel.text = "\(currentPage + 1)/\(bannersToDisplay.count - ((banners.count > 1) ? 2 : 0))"
            pageControl?.currentPage = currentPage
        }
    }
    
    private var offsetBegin: CGFloat { delegate?.bannerWidth() ?? UIScreen.main.bounds.width }
    
    private var isFirstTimeLoad = true
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
        
    public override func awakeFromNib() {
        super.awakeFromNib()
        view.addSubview(scv)
        scv.translatesAutoresizingMaskIntoConstraints = false
        scv.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scv.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scv.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scv.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        scv.delegate = self
        scv.backgroundColor = .clear
        scv.clipsToBounds = false
        scv.isPagingEnabled = true
        scv.showsHorizontalScrollIndicator = false
        
        view.addSubview(indicatorLabel)
        indicatorLabel.translatesAutoresizingMaskIntoConstraints = false
        indicatorLabel.heightAnchor.constraint(equalToConstant: 16).isActive = true
        indicatorLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -10).isActive = true
        indicatorLabel.widthAnchor.constraint(equalToConstant: 32).isActive = true
        indicatorLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -12).isActive = true
        indicatorLabel.textColor = .white
        indicatorLabel.textAlignment = .center
        indicatorLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        indicatorLabel.font = .systemFont(ofSize: 9)
        indicatorLabel.layer.masksToBounds = true
        indicatorLabel.layer.cornerRadius = 4
        
    }
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if banners.count > 1, isFirstTimeLoad {
            isFirstTimeLoad = false
            scv.setContentOffset(CGPoint(x: offsetBegin, y: 0), animated: false)
            startTimer()
        }
    }
    
    func clearAllBanners() {
        pauseTimer()
        bannersToDisplay.removeAll()
        stackView?.removeFromSuperview()
        pageControl?.removeFromSuperview()
        scv.contentOffset = .zero
    }
    
    public func reloadBanners() {
        guard let info = self.delegate?.bannerInfo() else { return }
        clearAllBanners()
        banners = info.banners
        bannersToDisplay.append(contentsOf: info.banners)
        if banners.count > 1 {
            bannersToDisplay.append(info.firstPlaceHolder)
            bannersToDisplay.insert(info.lastPlaceHolder, at: 0)
        }
        
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        scv.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: scv.topAnchor).isActive = true
        stack.bottomAnchor.constraint(equalTo: scv.bottomAnchor).isActive = true
        stack.leftAnchor.constraint(equalTo: scv.leftAnchor).isActive = true
        stack.rightAnchor.constraint(equalTo: scv.rightAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: scv.heightAnchor).isActive = true
        stack.widthAnchor.constraint(equalTo: scv.widthAnchor, multiplier: CGFloat(bannersToDisplay.count)).isActive = true
        stackView = stack

        for i in 0..<bannersToDisplay.count {
            let v = UIView()
            v.backgroundColor = .clear
            v.clipsToBounds = true
            stack.addArrangedSubview(v)
            
            
            let banner = bannersToDisplay[i].bannerView
            banner.isUserInteractionEnabled = true
            banner.tag = i
            
//            let img = BannerImageView(frame: v.bounds)
//            img.banner = banner
//            img.isUserInteractionEnabled = true
//            img.clipsToBounds = true
//            img.contentMode = .scaleAspectFill
//            img.backgroundColor = .clear
//            img.tag = i
//            img.kf.setImage(with: URL(string: banner.getImage() ?? ""), placeholder: self.delegate?.placeHolderImage() ?? UIImage())
//
            
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(bannerTaped(tap:)))
            banner.addGestureRecognizer(tap)
            v.addSubview(banner)
            banner.translatesAutoresizingMaskIntoConstraints = false
            banner.topAnchor.constraint(equalTo: v.topAnchor).isActive = true
            banner.bottomAnchor.constraint(equalTo: v.bottomAnchor).isActive = true
            banner.leftAnchor.constraint(equalTo: v.leftAnchor).isActive = true
            banner.rightAnchor.constraint(equalTo: v.rightAnchor).isActive = true
            
        }
        
        if let pageControl = delegate?.pageControl() as? UIPageControl {
            self.pageControl = pageControl
            view.addSubview(pageControl)
            pageControl.translatesAutoresizingMaskIntoConstraints = false
            pageControl.heightAnchor.constraint(equalToConstant: 26).isActive = true
            pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -12).isActive = true
            pageControl.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
            pageControl.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
            pageControl.hidesForSinglePage = true
            pageControl.currentPage = 0
            pageControl.numberOfPages = bannersToDisplay.count - (banners.count > 1 ? 2 : 0)
        }
        indicatorLabel.text = "1/\(bannersToDisplay.count - ((banners.count > 1) ? 2 : 0))"
        indicatorLabel.isHidden = delegate?.hideIndicatorLabel() ?? false
    }
    
    func startTimer() {
        guard let enable = delegate?.enableTimer(), enable else { return }
        if timer != nil {
            timer?.invalidate()
        }
        timer = Timer.scheduledTimer(withTimeInterval: bannerSwitchInterval, repeats: true, block: { [weak self] t in
            guard let self = self else { return }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.scrollToNextPage()
            }
        })
    }
    
    func pauseTimer() {
        timer?.invalidate()
    }
    
    @objc
    func bannerTaped(tap: UITapGestureRecognizer) {
        guard let banner = tap.view, let info = bannersToDisplay.filter({ $0.bannerView == banner }).first else {
            return
        }
        self.delegate?.bannderDidTapped(info: info)
    }
    
    @objc
    func scrollToNextPage() {
        pageIndex += 1
        scv.setContentOffset(CGPoint(x: CGFloat(pageIndex) * offsetBegin, y: 0), animated: true)
        if pageIndex == bannersToDisplay.count - 1 {
            pauseTimer()
            view.isUserInteractionEnabled = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) { [weak self] in
                guard let self = self else { return }
                self.scv.setContentOffset(CGPoint(x: self.offsetBegin, y: 0), animated: false)
                self.pageIndex = 1
                self.startTimer()
                self.view.isUserInteractionEnabled = true
            }
        }
    }
}

extension BannerViewController: UIScrollViewDelegate {
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.bounds.width)
        pageIndex = page
        if page == (bannersToDisplay.count - 1) {
            scrollView.setContentOffset(CGPoint(x: offsetBegin, y: 0), animated: false)
            pageIndex = 1
        }
        if page == 0 {
            scrollView.setContentOffset(CGPoint(x: offsetBegin * CGFloat(bannersToDisplay.count - 2), y: 0), animated: false)
            pageIndex = bannersToDisplay.count - 2
        }
    }
    
    public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        pauseTimer()
    }
    
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        startTimer()
    }
}
