//
//  Extensions.swift
//  
//
//  Created by Marc Liu on 2022/3/31.
//

import Foundation
import CoreLocation

public extension DispatchQueue {
    func syncExecuteOnMainQueue(closure: (() -> Void)) {
        if self === DispatchQueue.main && Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.sync {
                closure()
            }
        }
    }
    
    func asyncExecuteOnMainQueue(closure: @escaping (() -> Void)) {
        if self === DispatchQueue.main && Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async {
                closure()
            }
        }
    }
}

public extension Encodable {
    func saveToKeyChain(key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            do {
                try MLAppCoreManager.shared.keychain.set(encoded, key: key)
            }
            catch let error {
                MLLogger.log(type: .error, message: "Faile to store codable on key: \"\(key)\", error: \(error)")
            }
        }
    }
    
    func saveToUserDefault(key: String) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            UserDefaults.standard.set(encoded, forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
}

public extension Decodable {
    static func restoreFromKeychain<T: Decodable>(key: String) -> T? {
        let decoder = JSONDecoder()
        guard let savedData = try? MLAppCoreManager.shared.keychain.getData(key), let decoded = try? decoder.decode(T.self, from: savedData) else {
            MLLogger.log(type: .error, message: "Fail to restore \"\(T.self)\" from keychain with key: \"\(key)\". ")
            return nil
        }
        return decoded
    }
    
    static func restoreFromUserDefault<T: Decodable>(key: String) -> T? {
        let decoder = JSONDecoder()
        guard let savedData = UserDefaults.standard.object(forKey: key) as? Data, let decoded = try? decoder.decode(T.self, from: savedData) else {
            MLLogger.log(type: .error, message: "Fail to restore \"\(T.self)\" from user default with key: \"\(key)\". ")
            return nil
        }
        return decoded
    }
}

public extension CLLocation {
    private struct LocationCoords: Codable {
        var latitude: Double
        var longitude: Double
    }
    
    func saveToUserDefault(with key: String) {
        let location = LocationCoords(latitude: coordinate.latitude, longitude: coordinate.longitude)
        location.saveToUserDefault(key: key)
    }
    
    static func restoreFromUserDefault(with key: String) -> CLLocation? {
        guard let location: LocationCoords = .restoreFromUserDefault(key: key) else {
            return nil
        }
        return CLLocation(latitude: location.latitude, longitude: location.longitude)
    }
}
