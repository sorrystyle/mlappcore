//
//  File.swift
//  
//
//  Created by Marc Liu on 2023/5/2.
//

import Foundation

extension Data {
    mutating func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
