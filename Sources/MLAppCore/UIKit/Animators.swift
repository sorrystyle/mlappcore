//
//  Animators.swift
//  
//
//  Created by Marc Liu on 2022/11/21.
//

import Foundation
import UIKit

public class ScrollUpAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    let duration = 1.0
    var presenting = true
    var originFrame = CGRect.zero
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        duration
    }
    
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toView = transitionContext.view(forKey: .to)!
        let fromView = transitionContext.view(forKey: .from)!
        let height = UIScreen.main.bounds.height
        containerView.addSubview(toView)
        toView.frame = CGRect(origin: CGPoint(x: 0, y: height), size: toView.frame.size)
        fromView.frame = CGRect(origin: .zero, size: toView.frame.size)
        UIView.animate(
            withDuration: duration,
            delay: .zero,
            options: .curveEaseInOut,
            animations: {
                toView.frame = CGRect(origin: .zero, size: toView.frame.size)
                fromView.frame = CGRect(origin: CGPoint(x: 0, y: -height), size: fromView.frame.size)
            }, completion: { _ in
                transitionContext.completeTransition(true)
            }
        )
    }
}
