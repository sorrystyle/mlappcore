//
//  MLKeyboardListenableViewController.swift
//  
//
//  Created by Marc Liu on 2021/2/4.
//

import Foundation
import UIKit

open class MLKeyboardListenableViewController: UIViewController {
    var bottomSafeAreaPadding: CGFloat { UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0 }
    
    private weak var keyboardSyncConstraint: NSLayoutConstraint?
    public weak var keyboardConstraintScrollableView: UIView? {
        didSet {
            keyboardSyncConstraint = keyboardConstraintScrollableView?.getBottomConstraint()
            if let scrollable = keyboardConstraintScrollableView as? UIScrollView, onDragDismissKeyboard {
                scrollable.keyboardDismissMode = .onDrag
            }
        }
    }
    public var onDragDismissKeyboard = true
    
    deinit {
        MLAppCoreManager.shared.keyboardListenanbleManager.delegates.remove(self)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        MLAppCoreManager.shared.keyboardListenanbleManager.delegates.add(self)
        // Do any additional setup after loading the view.
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
}

extension MLKeyboardListenableViewController: MLKeyboardListenableManagerDelegate {
    public func keyboardDidShown(notification: NSNotification) {
        return
    }
    
    public func keyboardWillShow(notification: NSNotification) {
        guard let keyboardSyncConstraint = keyboardSyncConstraint else { return }
        let userInfo = notification.userInfo
        let duration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let curveInt = (userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
        let curve = UIView.AnimationOptions(rawValue: (curveInt ?? 0) << 16)
        let keyboardRect = userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect
        keyboardSyncConstraint.constant = keyboardRect.size.height - bottomSafeAreaPadding
        view.setNeedsLayout()
        UIView.animate(withDuration: duration, delay: 0, options: curve, animations: { [weak self] in
            guard let self = self else { return }
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    public func keyboardWillDismiss(notification: NSNotification) {
        guard let keyboardSyncConstraint = keyboardSyncConstraint else { return }
        let userInfo = notification.userInfo
        let duration = userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as! TimeInterval
        let curveInt = (userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber)?.uintValue
        let curve = UIView.AnimationOptions(rawValue: (curveInt ?? 0) << 16)
        keyboardSyncConstraint.constant = 0
        view.setNeedsLayout()
        UIView.animate(withDuration: duration, delay: 0, options: curve, animations: { [weak self] in
            guard let self = self else { return }
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
}
