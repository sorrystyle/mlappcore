//
//  UILabelExtensions.swift
//  
//
//  Created by Marc Liu on 2021/11/10.
//

import Foundation
import UIKit

public extension UILabel {
    func currentNumberOfLines() -> Int {
        layoutIfNeeded()
        return Int(ceil(CGFloat(bounds.size.height) / self.font.lineHeight))
    }
}
