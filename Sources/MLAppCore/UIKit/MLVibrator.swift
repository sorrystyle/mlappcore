//
//  MLVibrator.swift
//  
//
//  Created by Marc Liu on 2021/2/4.
//
import AudioToolbox
import UIKit

public enum MLVibrateStyle {
    case alert
    case longVibrate
    case heavy
    case medium
    case light
    case system
}

public class MLVibrator {
    static let shared = MLVibrator()
    let hapticHeavy = UIImpactFeedbackGenerator(style: .heavy)
    let hapticMeduim = UIImpactFeedbackGenerator(style: .medium)
    let hapticLight = UIImpactFeedbackGenerator(style: .light)

    public static func vibrate() {
        MLVibrator.shared.hapticLight.impactOccurred()
    }
    
    public static func vibrate(style: MLVibrateStyle) {
        switch style {
        case .alert:
            MLVibrator.shared.hapticHeavy.impactOccurred()
            Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { _ in
                MLVibrator.shared.hapticLight.impactOccurred()
            }
        case .longVibrate:
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        case .heavy:
            MLVibrator.shared.hapticHeavy.impactOccurred()
        case .medium:
            MLVibrator.shared.hapticMeduim.impactOccurred()
        case .light:
            MLVibrator.shared.hapticLight.impactOccurred()
        case .system:
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        }
    }
}
