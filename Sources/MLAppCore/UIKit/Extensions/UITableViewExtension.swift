//
//  UITableViewExtension.swift
//  
//
//  Created by Marc Liu on 2021/2/4.
//

import UIKit

public extension UITableView {
    func scrollToBottom(animated: Bool = true) {
        for sectionIndex in stride(from: numberOfSections - 1, to: -1, by: -1) {
            let rows = numberOfRows(inSection: sectionIndex)
            if rows > 0 {
                DispatchQueue.main.asyncExecuteOnMainQueue { [weak self] in
                    guard let self = self else { return }
                    let indexPath = IndexPath(row: rows - 1, section: sectionIndex)
                    self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                }
                return
            }
        }
    }
}

