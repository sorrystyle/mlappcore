//
//  File.swift
//  
//
//  Created by Marc Liu on 2022/3/22.
//

import Foundation
import UIKit

let windlowLocker = NSLock()

struct CoreConstant {
    
    enum UserDefaultKey: String, CaseIterable {
        case floatingWindowKey
    }
}

struct Constant {
    struct UI {
        static let defaultCornerRadius: CGFloat = 12
        static let notificationWindowIconCornerRadius: CGFloat = 4
        static let animationDuration: TimeInterval = 0.25
        static let windowAutomaticallyDismissingInterval: TimeInterval = 2.5
        static let windowInterruptedDismissingInterval: TimeInterval = 1
        
        static var topPadding: CGFloat { UIApplication.shared.windows.first?.safeAreaInsets.top ?? 0 }
        static var bottomPadding: CGFloat { UIApplication.shared.windows.first?.safeAreaInsets.bottom ?? 0 }
    }
}
