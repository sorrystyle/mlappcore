//
//  MMLogger.swift
//  
//
//  Created by Marc Liu on 2022/3/31.
//

import Foundation

public enum MLLogType: String {
    case debug
    
    case info
    case warning
    case error
    case action
    case canceled
    case apiSuccess
    case apiError
    
    var emoji: String {
        switch self {
        case .debug:
            return "🐞🐞🐞"
        case .info:
            return "📕📕📕"
        case .warning:
            return "⚠️⚠️⚠️"
        case .error:
            return "❌❌❌"
        case .action:
            return "🛠🛠🛠"
        case .canceled:
            return "🚫🚫🚫"
        case .apiSuccess:
            return "✅✅✅"
        case .apiError:
            return "🈳🈳🈳"
        }
    }
}

public class MLLogger {
    public static func log(type: MLLogType, message: String) {
        let now = Date()
        let format = MLDateFormat.TaipeiTimeFull
        let timeStamp = format.string(from: now)
        let logLine = "[MMLog] \(timeStamp) [\(type.emoji) \(type.rawValue.uppercased())] " + message
        print(logLine)
    }
}
