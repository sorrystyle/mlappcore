//
//  NibLoadable.swift
//  
//
//  Created by Marc Liu on 2022/10/7.
//

import Foundation
import UIKit

public protocol NibLoadable: AnyObject {
    static var nib: UINib { get }
}

public extension NibLoadable {
    static var nib: UINib { return UINib(nibName: String(describing: self), bundle: Bundle(for: self)) }
}

public extension NibLoadable where Self: UIView {
    static func loadFromNib() -> Self {
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else {
            fatalError("The nib \(nib) expected its root view to be of type \(self)")
        }
        return view
    }
    
    static func loadFromNibAndAddToParent(parentView: UIView) -> Self {
        let me = Self.loadFromNib()
        parentView.addSubview(me)
        me.translatesAutoresizingMaskIntoConstraints = false
        me.topAnchor.constraint(equalTo: parentView.topAnchor).isActive = true
        me.bottomAnchor.constraint(equalTo: parentView.bottomAnchor).isActive = true
        me.leftAnchor.constraint(equalTo: parentView.leftAnchor).isActive = true
        me.rightAnchor.constraint(equalTo: parentView.rightAnchor).isActive = true
        return me
    }
}
