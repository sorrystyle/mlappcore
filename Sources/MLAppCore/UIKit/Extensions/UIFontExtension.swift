//
//  UIFontExtension.swift
//  
//
//  Created by Marc Liu on 2022/11/8.
//

import UIKit

public extension UIFont {
    func fontWeight(weight: UIFont.Weight) -> UIFont {
        let fontDescriptor = UIFontDescriptor(fontAttributes: [
                    UIFontDescriptor.AttributeName.size: pointSize,
                    UIFontDescriptor.AttributeName.family: familyName
                ])
        let weightedFontDescriptor = fontDescriptor.addingAttributes([
                    UIFontDescriptor.AttributeName.traits: [
                        UIFontDescriptor.TraitKey.weight: weight
                    ]
                ])
        return UIFont(descriptor: weightedFontDescriptor, size: 0)
    }
}
