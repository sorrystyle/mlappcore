//
//  UIImageExtension.swift
//  
//
//  Created by Marc Liu on 2021/10/28.
//

import Foundation
import UIKit

public extension UIImage {
    func resize(size: CGSize) -> UIImage? {
        let rect = CGRect(origin: .zero, size: size)
            
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
