//
//  UIColorExtension.swift
//  
//
//  Created by Marc Liu on 2021/2/4.
//
import UIKit

public extension UIColor {
    func image() -> UIImage {
        let size: CGSize = CGSize(width: 1, height: 1)
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
    
    static var randomColor: UIColor {
        let red = CGFloat(arc4random()%101)/100
        let green = CGFloat(arc4random()%101)/100
        let blue = CGFloat(arc4random()%101)/100
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}
