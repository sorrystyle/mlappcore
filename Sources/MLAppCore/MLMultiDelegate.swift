//
//  MFMultiDelegate.swift
//  
//
//  Created by Marc Liu on 2021/5/4.
//

import Foundation

public final class Weak: Equatable {
    weak var value: AnyObject?

    init(value: AnyObject) {
        self.value = value
    }

    public static func == (lhs: Weak, rhs: Weak) -> Bool {
        return lhs.value === rhs.value
    }
}

public class MLAutoLocker {
    var lock: NSLocking
    var dummy: Bool = false

    init(lock: NSLocking) {
        self.lock = lock
        self.lock.lock()
    }

    // To avoid warning
    func retain() {
        dummy = true
    }

    deinit {
        self.lock.unlock()
    }
}

public final class MLMultiDelegate<T> {
    private var delegates = [Weak]()
    private let lock = NSLock()
    
    public init() {}

    public func add(_ delegate: T) {
        if Mirror(reflecting: delegate).subjectType is AnyClass {
            lock.lock()
            let observer = delegate as AnyObject
            delegates = delegates.filter { $0.value != nil && $0.value !== observer }
            let weakValue = Weak(value: observer)
            delegates.append(weakValue)
            lock.unlock()
        } else {
            print("💩 Multicast delegates do not support value types")
        }
    }

    public func remove(_ delegate: T) {
        if Mirror(reflecting: delegate).subjectType is AnyClass {
            lock.lock()
            let observer = delegate as AnyObject
            delegates = delegates.filter { $0.value != nil && $0.value !== observer }
            lock.unlock()
        }
    }

    public func invoke(_ invocation: (T) -> Void) {
        lock.lock()
        let delegatesArray = delegates
        lock.unlock()

        var unusedDelegates = [Weak]()
        for delegate in delegatesArray {
            if let delegate = delegate.value as? T {
                invocation(delegate)
            } else {
                unusedDelegates.append(delegate)
            }
        }

        removeObjects(with: unusedDelegates)
    }

    public func allDelegates() -> [T?] {
        lock.lock()
        let delegatesArray = delegates
        lock.unlock()

        return delegatesArray.map {
            return $0.value as? T
        }
    }

    private func removeObjects(with delegates: [Weak]) {
        for delegate in delegates {
            removeObject(with: delegate)
        }
    }

    private func removeObject(with delegate: Weak) {
        let autoLock = MLAutoLocker(lock: lock)
        autoLock.retain()
        guard let index = delegates.firstIndex(of: delegate) else {
            return
        }

        delegates.remove(at: index)
    }

    internal func numberOfDelegates() -> Int {
        return delegates.count
    }
}
