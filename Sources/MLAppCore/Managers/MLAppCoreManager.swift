import Foundation
#if !os(macOS)
import UIKit
#endif
import KeychainAccess

public enum AppLifeCycleMode {
    case foreground
    case background
    case willBeTerminated
}

public protocol MLAppCoreManagerDelegate: AnyObject {
    func appDidEnterBackground()
    func appDidBecomeActive()
    func appWillBeTerminate()
}

public class MLAppCoreManager {
    public static let shared = MLAppCoreManager()
    
    public let delegates = MLMultiDelegate<MLAppCoreManagerDelegate>()
    
    public var reachabilityManager = MLReachabilityManager(reachabilityTestHost: "google.com")
    public var keyboardListenanbleManager = MLKeyboardListenableManager()
    
    private var appIsInBackground = false
    
    let keychain = Keychain(service: Bundle.main.bundleIdentifier ?? "")
    public let deivceDescripiton = UIDevice.current.modelDescription
    
    
    public func initialize(reachabilityTestingHost: String) {
        reachabilityManager = MLReachabilityManager(reachabilityTestHost: reachabilityTestingHost)
        delegates.add(reachabilityManager)
    }
    
    deinit {
        delegates.remove(reachabilityManager)
    }
    
    public func appSwitchLifeCycle(to mode: AppLifeCycleMode) {
        switch mode {
        case .foreground:
            if appIsInBackground {
                appIsInBackground = false
                delegates.invoke({ $0.appDidBecomeActive() })
                MLLogger.log(type: .debug, message: "Broadcast App Become Active Event.")
            }
        case .background:
            appIsInBackground = true
            delegates.invoke({ $0.appDidEnterBackground() })
            MLLogger.log(type: .debug, message: "Broadcast App Entered Background.")
        case .willBeTerminated:
            delegates.invoke({ $0.appWillBeTerminate() })
            MLLogger.log(type: .warning, message: "App is about to be terminated.")
        }
    }
    
    public func cleanUpKeyChain() {
        try? keychain.removeAll()
    }
    
    public func removeKeyChainValue(forKey key: String) {
        try? keychain.remove(key)
    }
}
