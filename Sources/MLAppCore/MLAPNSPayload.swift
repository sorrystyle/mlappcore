//
//  MMAPNSPayload.swift
//  
//
//  Created by Marc Liu on 2022/9/13.
//

// Your Push payload should use the following format.
// {    "aps":  {
//          "alert": {
//              "title": "Title",
//              "body": "Body"
//          },
//          "badge": 99,
//          "sound": "default"
//      },
//      "info": {
//          "icon": "https://www.iconsdb.com/icons/preview/red/free-badge-xxl.png",
//          "info": "Custom Infomation."
//      }
// }

import Foundation

public enum MLAPNSNotificationSound: String, Decodable {
    case standard = "default"
}

public struct MLAPNSNotificationAlert: Decodable {
    public var title: String?
    public var body: String?
}

public struct MLAPNSNotificationAPS: Decodable {
    public var alert: MLAPNSNotificationAlert?
    public var badge: Int = 0
    public var sound = MLAPNSNotificationSound.standard
}

public struct MLAPNSCustomInfo: Decodable {
    public var icon: String?
    public var info: String?
}

public struct MLAPNSNotificationPayload: Decodable {
    public var aps: MLAPNSNotificationAPS?
    public var info: MLAPNSCustomInfo?
    
    public init(textNotification: [AnyHashable: Any]) {
        if let apsData = textNotification["aps"] as? [AnyHashable: Any], let notification = apsData["alert"] as? String {
            let decoder = JSONDecoder()
            if let data = notification.data(using: .utf8), let result = try? decoder.decode(MLAPNSNotificationPayload.self, from: data) {
                self = result
            }
        }
    }
    
    public init(JSONNotification: [AnyHashable: Any]) {
        let decoder = JSONDecoder()
        if let dict = JSONNotification["aps"], let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted), let result = try? decoder.decode(MLAPNSNotificationAPS.self, from: jsonData) {
            aps = result
        }
        if let dict = JSONNotification["info"], let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted), let result = try? decoder.decode(MLAPNSCustomInfo.self, from: jsonData) {
            info = result
        }
    }
}
