//
//  MMOperationTask.swift
//
//  Created by Marc Liu on 2022/5/5.
//

import Foundation

open class MLOperationTask {
    let identifier: String

    private weak var taskSemaphore: DispatchSemaphore?
    private var workItem: DispatchWorkItem?
    private var state: TaskState = .normal
    private let action: (@escaping() -> Void) -> Void
    private var placeholder: Bool = false

    fileprivate var complection: ((MLOperationTask) -> Void)?

    private enum TaskState {
        case normal
        case cancel
    }

    public init(identifier: String? = nil, action: @escaping (@escaping () -> Void) -> Void) {
        self.identifier = identifier ?? UUID().uuidString
        self.action = action
    }

    public func startTask(queue: DispatchQueue, semaphore: DispatchSemaphore) {
        taskSemaphore = semaphore
        state = .normal
        workItem = DispatchWorkItem { [weak self] in
            self?.action { [weak self] in
                guard let self = self, self.state != .cancel else { return }
                self.complection?(self)
                semaphore.signal()
            }
        }
        queue.async { [weak self] in
            if let self = self, self.state == .cancel { return }
            semaphore.wait()
            self?.workItem?.perform()
        }
    }

    public func cancel() {
        state = .cancel
        workItem?.cancel()
        taskSemaphore?.signal()
    }
}
