//
//  MVReachabilityManager.swift
//  
//
//  Created by Marc Liu on 2021/5/5.
//

import Foundation
import Reachability

public protocol MLReachabilityManagerDelegate: AnyObject {
    func appLostNetworkConnection()
    func appResumeNetWorkConnection(type: Reachability.Connection)
}

public class MLReachabilityManager {
    public var delegates = MLMultiDelegate<MLReachabilityManagerDelegate>()
    
    private var reachability: Reachability!
    
    public var connectionType: Reachability.Connection {
        reachability.connection
    }
    
    init(reachabilityTestHost: String) {
        reachability = try! Reachability(hostname: reachabilityTestHost)
        reachability.whenReachable = { [weak self] reachability in
            guard let self = self else { return }
            MLLogger.log(type: .info, message: "Network established with \(reachability.connection).")
            self.delegates.invoke({ $0.appResumeNetWorkConnection(type: reachability.connection) })
        }
        reachability.whenUnreachable = { [weak self] _ in
            guard let self = self else { return }
            MLLogger.log(type: .warning, message: "No Network Connection!!!")
            self.delegates.invoke({ $0.appLostNetworkConnection() })
        }
        do {
            try reachability.startNotifier()
        } catch {
            MLLogger.log(type: .warning, message: "Unable to start notifier")
        }
    }
    
    deinit {
    }
}

extension MLReachabilityManager: MLAppCoreManagerDelegate {
    public func appWillBeTerminate() {
        return
    }
    
    public func appDidEnterBackground() {
        MLLogger.log(type: .warning, message: "Stop monitoring network status.")
        reachability.stopNotifier()
    }
    
    public func appDidBecomeActive() {
        MLLogger.log(type: .warning, message: "Start monitoring network status...")
        do {
            try reachability.startNotifier()
        } catch {
            MLLogger.log(type: .warning, message: "Unable to start notifier")
        }
    }
}
