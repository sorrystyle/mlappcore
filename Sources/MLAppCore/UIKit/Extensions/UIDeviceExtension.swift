//
//  File.swift
//  
//
//  Created by Marc Liu on 2022/9/30.
//

import Foundation
import UIKit

extension UIDevice {
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        return identifier
    }
    
    var modelDescription: String {
        do {
            if let filePath = Bundle.module.path(forResource: "ModelMap", ofType: "json") {
                MLLogger.log(type: .info, message: "Read model map from: \"\(filePath)\"")
                let fileUrl = URL(fileURLWithPath: filePath)
                let data = try Data(contentsOf: fileUrl)
                let decodedData = try JSONDecoder().decode([String: String].self, from: data)
                return decodedData[UIDevice.current.modelName] ?? "Unknown Apple Device"
            }
        } catch {
            print("error: \(error)")
        }
        return "Fail to get device map."
    }
}
