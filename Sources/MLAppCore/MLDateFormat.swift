//
//  MMDateFormat.swift
//  
//
//  Created by Marc Liu on 2022/3/9.
//

import Foundation

public enum MLDateFormat {
    case TaipeiTimeFull
    case UTCTimeFull
    case YMD
    case custom(isTaipeiTime: Bool, formatString: String)
    
    var formatter: DateFormatter {
        let formatter = DateFormatter()
        switch self {
        case .TaipeiTimeFull:
            formatter.timeZone = TimeZone(identifier: "Asia/Taipei")
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        case .UTCTimeFull:
            formatter.timeZone = TimeZone(abbreviation: "UTC")
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        case .YMD:
            formatter.timeZone = TimeZone.current
            formatter.dateFormat = "yyyyMMdd"
        case .custom(let isTaipeiTime, let formatString):
            formatter.timeZone = isTaipeiTime ? TimeZone(identifier: "Asia/Taipei") : TimeZone(abbreviation: "UTC")
            formatter.dateFormat = formatString
        }
        return formatter
    }
    
    public func string(from: Date) -> String {
        return formatter.string(from: from)
    }
    
    public func date(from: String) -> Date? {
        return formatter.date(from: from)
    }
}
