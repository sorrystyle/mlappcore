//
//  NotificationSound.swift
//  NotiMe
//
//  Created by Marc LIU on 2020/4/1.
//  Copyright © 2020 Marc LIU. All rights reserved.
//

import AVFoundation
import UIKit

public class MLSound {
    static let shared = MLSound()
    var defaultSound: URL? = URL(fileURLWithPath: Bundle.module.path(forResource: "m0", ofType: "aiff") ?? "")
    private var player: AVAudioPlayer?
    public class func play() {
        if let sound = shared.defaultSound {
            shared.player = try? AVAudioPlayer(contentsOf: sound)
            shared.player?.prepareToPlay()
            shared.player?.play()
        }
    }
    public class func play(fileUrl: URL?) {
        if let sound = fileUrl {
            shared.player = try? AVAudioPlayer(contentsOf: sound)
            shared.player?.prepareToPlay()
            shared.player?.play()
        }
    }
}
