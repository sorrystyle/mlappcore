//
//  MFLiveData.swift
//  
//
//  Created by Marc Liu on 2021/5/5.
//

import Foundation

public class MLLiveData<T> {
    public typealias CompletionHandler = ((T) -> Void)
    public private(set) var value: T {
        didSet {
            self.notify()
        }
    }

    private var observers = [String: CompletionHandler]()

    public init(_ value: T) {
        self.value = value
    }

    public func just(_ value: T) {
        self.value = value
    }

    public func addObserver(_ observer: AnyObject, completionHandler: @escaping CompletionHandler) {
        DispatchQueue.main.sync {
            observers[observer.description] = completionHandler
        }
    }

    public func addAndNotify(observer: AnyObject, completionHandler: @escaping CompletionHandler) {
        DispatchQueue.main.sync {
            addObserver(observer, completionHandler: completionHandler)
            observers[observer.description]?(value)
        }
    }

    public func removeObserver(_ observer: AnyObject) {
        DispatchQueue.main.syncExecuteOnMainQueue {
            observers.removeValue(forKey: observer.description)
        }
    }

    public func notify() {
        observers.forEach({ $0.value(value) })
    }

    deinit {
        observers.removeAll()
    }
}
