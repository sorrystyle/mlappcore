//
//  UIViewExtensions.swift
//  
//
//  Created by Marc Liu on 2021/2/4.
//

import UIKit
import QuartzCore

public extension UIView {
    func pulsatile(duration: TimeInterval = 0.2, minScale: Float = 1, maxScale: Float = 1.5, repeatCount: Float = 1) {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = duration/Double(repeatCount)
        pulse.fromValue = minScale
        pulse.toValue = maxScale
        pulse.autoreverses = true
        pulse.repeatCount = repeatCount
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0

        layer.add(pulse, forKey: "pulse")
    }

    func flash(duration: TimeInterval = 0.15, repeatCount: Float = 3) {
        let flash = CABasicAnimation(keyPath: "opacity")
        flash.duration = duration/Double(repeatCount)
        flash.fromValue = 1
        flash.toValue = 0.1
        flash.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        flash.autoreverses = true
        flash.repeatCount = repeatCount
        layer.add(flash, forKey: nil)
    }

    func shake(duration: TimeInterval = 0.1, repeatCount: Float = 2, distance: CGFloat = 5) {
        let shake = CABasicAnimation(keyPath: "position")
        shake.duration = duration / Double(repeatCount)
        shake.repeatCount = repeatCount
        shake.autoreverses = true
        shake.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)

        let fromPoint = CGPoint(x: center.x - distance, y: center.y)
        let fromValue = NSValue(cgPoint: fromPoint)

        let toPoint = CGPoint(x: center.x + distance, y: center.y)
        let toValue = NSValue(cgPoint: toPoint)

        shake.fromValue = fromValue
        shake.toValue = toValue

        layer.add(shake, forKey: "position")
    }
}

public extension UIView {
    func getBottomConstraint() -> NSLayoutConstraint? {
        guard let superview = superview else { return nil }
        for constraint in superview.constraints {
            if isBottomConstraint(constraint: constraint) {
                return constraint
            }
        }
        return nil
    }
    
    private func isBottomConstraint(constraint: NSLayoutConstraint) -> Bool {
        return firstItemMatchesBottomConstraint(constraint: constraint) || secondItemMatchesBottomConstraint(constraint: constraint)
    }
    
    private func firstItemMatchesBottomConstraint(constraint: NSLayoutConstraint) -> Bool {
        guard let item = constraint.firstItem as? UIView else {
            return false
        }
        return item == self && constraint.firstAttribute == NSLayoutConstraint.Attribute.bottom
    }
    
    private func secondItemMatchesBottomConstraint(constraint: NSLayoutConstraint) -> Bool {
        guard let item = constraint.secondItem as? UIView else {
            return false
        }
        return item == self && constraint.secondAttribute == NSLayoutConstraint.Attribute.bottom
    }
}

public extension UIView {
    
    func addGradientLayer(isHorizontal: Bool, startColor: UIColor, endColor: UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        gradient.locations = [0, 1]
        gradient.startPoint = .zero
        gradient.endPoint = (isHorizontal ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 1))
        layer.addSublayer(gradient)
    }
    
    func add3ColorDiagonalGradientLayer(startColor: UIColor,  midColor: UIColor, endColor: UIColor) {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [startColor.cgColor, midColor.cgColor, endColor.cgColor]
        gradient.locations = [0, 0.5, 1]
        gradient.startPoint = CGPoint(x: 1, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 1)
        layer.addSublayer(gradient)
    }
}

protocol ViewBoundsObserving: AnyObject {
    func boundsDidChange(_ view: BoundsObservableView, from previousBounds: CGRect);
}

public class BoundsObservableView: UIView {
    
    weak var boundsDelegate: ViewBoundsObserving?
    
    public var onSizeChangeAction: ((CGSize)->Void)?
    
    private var previousBounds: CGRect = .zero
    
    public override func layoutSubviews() {
        if (bounds != previousBounds) {
            print("Bounds changed from \(previousBounds) to \(bounds)")
            boundsDelegate?.boundsDidChange(self, from: previousBounds)
            previousBounds = bounds
            onSizeChangeAction?(bounds.size)
        }
        
        // UIView's implementation will layout subviews for me using Auto Resizing mask or Auto Layout constraints.
        super.layoutSubviews()
    }
}

public extension UIView {
    func dropShadow(shadowColor: UIColor = .black, opacity: Float = 0.2, radius: CGFloat = 8, offset: CGSize = CGSize(width: 3, height: 3)) {
        layer.masksToBounds = false
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = radius
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
}
