//
//  MLWindowManager.swift
//  
//
//  Created by Marc Liu on 2021/2/4.
//

import Foundation
import UIKit
import SwiftUI

public let topPadding = Constant.UI.topPadding
public let bottomPadding = Constant.UI.bottomPadding

public enum MLWindowPresentingStyle {
    case none
    case fadein
    case upward
    case downward
    case leftward
    
    var defaultDismissingStyle: MLWindowDismissingStyle {
        switch self {
        case .none:
            return .none
        case .fadein:
            return .fadeout
        case .upward:
            return .downward
        case .downward:
            return .upward
        case .leftward:
            return .rightward
        }
    }
}

public enum MLWindowDismissingStyle {
    case none
    case fadeout
    case downward
    case upward
    case rightward
}

public enum MLWindowScene {
    case app
    case floating
    case openning
    case datePicker
    case picker
    case residencePicker
    case audioPlayer
    case toast
    case notification
    case networkStatus
    case news
    case resgistrationBlocker
    case alert
    case feedback
    case passcode
    
    var windowLevel: UIWindow.Level {
        switch self {
        case .app:
            return .normal
        case .floating:
            return .normal + 50
        case .datePicker:
            return .normal + 60
        case .picker:
            return .normal + 70
        case .residencePicker:
            return .normal + 80
        case .audioPlayer:
            return .normal + 100
        case .toast:
            return .statusBar + 10
        case .notification:
            return .statusBar + 20
        case .networkStatus:
            return .statusBar + 30
        case .news:
            return .statusBar + 40
        case .resgistrationBlocker:
            return .statusBar + 50
        case .alert:
            return .alert
        case .feedback:
            return .alert + 10
        case .passcode:
            return .alert + 9000
        case .openning:
            return .alert + 9999
        }
    }
    
    var frame: CGRect {
        switch self {
        case .networkStatus, .toast:
            return CGRect(x: 0, y: topPadding, width: UIScreen.main.bounds.width, height: 44)
        case .notification:
            return CGRect(x: 0, y: topPadding, width: UIScreen.main.bounds.width, height: 72)
        case .floating:
            if let rect: CGRect = .restoreFromUserDefault(key: CoreConstant.UserDefaultKey.floatingWindowKey.rawValue) {
                return rect
            }
            return CGRect(x: UIScreen.main.bounds.width - 20 - 60, y: UIScreen.main.bounds.height - bottomPadding - 60 , width: 60, height: 60)
        case .feedback:
            return CGRect(x: (UIScreen.main.bounds.width - 80) / 2, y: (UIScreen.main.bounds.height - 80) / 2, width: 80, height: 80)
        default:
            return UIScreen.main.bounds
        }
    }
    
    var window: UIWindow? {
        switch self {
        case .app:
            return MLWindowManager.shared.appWindow
        case .notification:
            return nil
        case .networkStatus:
            return MLWindowManager.shared.networkStatusWindow
        case .toast:
            return MLWindowManager.shared.toastWindow
        case .news:
            return MLWindowManager.shared.newsWindow
        case .resgistrationBlocker:
            return MLWindowManager.shared.registrationWindow
        case .audioPlayer:
            return MLWindowManager.shared.audioPlayerWindow
        case .alert:
            return MLWindowManager.shared.alerWindow
        case .feedback:
            return MLWindowManager.shared.feedbackWindow
        case .passcode:
            return MLWindowManager.shared.passcodeWindow
        case .openning:
            return MLWindowManager.shared.openningWindow
        case .floating:
            return MLWindowManager.shared.floatingWindow
        case .datePicker:
            return MLWindowManager.shared.datePickerWindow
        case .picker:
            return MLWindowManager.shared.pickerWindow
        case .residencePicker:
            return MLWindowManager.shared.residencePickerWindow
        }
    }
}

public class MLWindow: UIWindow {
    var windowId = UUID().uuidString
    public var scene: MLWindowScene = .app
    private weak var dismissingTimer: Timer?
    private var halfHeight: CGFloat = 0
    private var tapAction: (()->Void)?
    
    private var floatingBlockView = UIView()
    private var isMinimized = true {
        didSet {
            for gesture in floatingWindowGestures {
                gesture.isEnabled = isMinimized
            }
        }
    }
    var floatingWindowGestures = [UIGestureRecognizer]()
    
    var presentingStyle: MLWindowPresentingStyle = .none {
        didSet {
            dismissingStyle = presentingStyle.defaultDismissingStyle
        }
    }
    var dismissingStyle: MLWindowDismissingStyle = .none
    
    init(scene: MLWindowScene, tapAction: (()->Void)? = nil) {
        super.init(frame: .zero)
        self.scene = scene
        self.frame = scene.frame
        self.halfHeight = frame.size.height / 2
        self.backgroundColor = .clear
        self.windowLevel = scene == .notification ? MLWindowManager.currentNotificationWindowLevel : scene.windowLevel
        self.windowScene = UIApplication.shared.windows.first?.windowScene
        self.isHidden = true
        self.layer.masksToBounds = true
        self.tapAction = tapAction
        if scene == .notification {
            let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(pan:)))
            self.addGestureRecognizer(pan)
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(tap:)))
            self.addGestureRecognizer(tap)
            tap.shouldRequireFailure(of: pan)
        }
        if scene == .floating {
            self.layer.cornerRadius = self.frame.height / 2 
            floatingBlockView.backgroundColor = .yellow
            let pan = UIPanGestureRecognizer(target: self, action: #selector(handleFloatingWindowPan(pan:)))
            self.addGestureRecognizer(pan)
            floatingWindowGestures.append(pan)
            let tap = UITapGestureRecognizer(target: self, action: #selector(handleFloatingWindowTap(tap:)))
            self.addGestureRecognizer(tap)
            floatingWindowGestures.append(tap)
            tap.shouldRequireFailure(of: pan)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(completion: @escaping () -> Void) {
        switch presentingStyle {
        case .none:
            alpha = 1
            isHidden = false
            completion()
            automaticallyCloseIfNeeded()
        case .fadein:
            alpha = 0
            isHidden = false
            UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                self?.alpha = 1
            } completion: { [weak self] (_) in
                completion()
                guard let self = self else { return }
                self.automaticallyCloseIfNeeded()
            }
        case .upward:
            var f = frame
            f.origin = CGPoint(x: f.origin.x, y: f.size.height)
            frame = f
            alpha = 1
            isHidden = false
            UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                guard let self = self else { return }
                let frame = self.frame
                f.origin = .zero
                self.frame = f
            } completion: { [weak self] (_) in
                completion()
                guard let self = self else { return }
                self.automaticallyCloseIfNeeded()
            }
        case .downward:
            var f = frame
            f.origin = CGPoint(x: f.origin.x, y: -f.size.height)
            frame = f
            alpha = 1
            isHidden = false
            UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                guard let self = self else { return }
                let frame = self.frame
                f.origin = CGPoint(x: 0, y: topPadding)
                self.frame = f
            } completion: { [weak self] (_) in
                completion()
                guard let self = self else { return }
                self.automaticallyCloseIfNeeded()
            }
        case .leftward:
            var f = frame
            f.origin = CGPoint(x: f.size.width, y: f.origin.y)
            frame = f
            alpha = 1
            isHidden = false
            UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                guard let self = self else { return }
                let frame = self.frame
                f.origin = .zero
                self.frame = f
            } completion: { [weak self] (_) in
                completion()
                guard let self = self else { return }
                self.automaticallyCloseIfNeeded()
            }
        }
    }
    
    func automaticallyCloseIfNeeded() {
        if scene == .notification {
            dismissingStyle = .upward
            dismissingTimer = Timer.scheduledTimer(timeInterval: Constant.UI.windowAutomaticallyDismissingInterval, target: self, selector: #selector(dismiss), userInfo: nil, repeats: false)
        }
    }
    
    @objc
    func dismiss(enableTapAction: Bool = false) {
        func releaseNotificationWindow() {
            if scene == .notification {
                dismissingTimer?.invalidate()
                dismissingTimer = nil
                MLWindowManager.shared.locker.lock()
                MLWindowManager.currentNotificationWindowLevel -= 1
                MLWindowManager.shared.notificationWindows[windowId] = nil
                MLWindowManager.shared.locker.unlock()
                if enableTapAction {
                    tapAction?()
                }
            }
        }
        
        DispatchQueue.main.asyncExecuteOnMainQueue { [weak self] in
            guard let self = self else { return }
            switch self.dismissingStyle {
            case .none:
                self.alpha = 0
                self.isHidden = true
                self.rootViewController = nil
                releaseNotificationWindow()
            case .fadeout:
                UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                    guard let sself = self else { return }
                    sself.alpha = 0
                } completion: { [weak self] (_) in
                    guard let sself = self else { return }
                    sself.isHidden = true
                    sself.rootViewController = nil
                    releaseNotificationWindow()
                }
            case .downward:
                var f = self.frame
                f.origin = CGPoint(x: f.origin.x, y: f.size.height)
                UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                    guard let sself = self else { return }
                    sself.frame = f
                } completion: { [weak self] (_) in
                    guard let sself = self else { return }
                    sself.isHidden = true
                    sself.rootViewController = nil
                    releaseNotificationWindow()
                }
            case .upward:
                UIViewPropertyAnimator.runningPropertyAnimator(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                    guard let sself = self else { return }
                    var f = sself.frame
                    f.origin = CGPoint(x: f.origin.x, y: -f.size.height)
                    sself.frame = f
                } completion: { [weak self] _ in
                    guard let sself = self else { return }
                    sself.isHidden = true
                    sself.rootViewController = nil
                    releaseNotificationWindow()
                }
            case .rightward:
                var f = self.frame
                f.origin = CGPoint(x: f.size.width, y: f.origin.y)
                UIView.animate(withDuration: Constant.UI.animationDuration, delay: 0, options: .curveEaseInOut) { [weak self] in
                    guard let sself = self else { return }
                    sself.frame = f
                } completion: { [weak self] (_) in
                    guard let sself = self else { return }
                    sself.isHidden = true
                    sself.rootViewController = nil
                    releaseNotificationWindow()
                }
            }
        }
    }
    
    @objc
    func handlePan(pan: UIPanGestureRecognizer) {
        
        if pan.state == UIGestureRecognizer.State.began || pan.state == UIGestureRecognizer.State.changed {
            dismissingTimer?.invalidate()
            dismissingTimer = nil

            let translation = pan.translation(in: self)
            let newY = center.y + translation.y
            guard newY < halfHeight + topPadding else { return }
            center = CGPoint(x: center.x, y: newY)
            pan.setTranslation(.zero, in: self)
            if center.y < topPadding {
                pan.isEnabled = false
                dismiss()
            }
        }
        if pan.state == UIGestureRecognizer.State.ended {
            DispatchQueue.main.async {
                UIView.animate(withDuration: Constant.UI.animationDuration, animations: { [weak self] in
                    guard let self = self else { return }
                    self.center = CGPoint(x: self.center.x, y: self.halfHeight + topPadding)
                }) { [weak self] _ in
                    guard let self = self else { return }
                    self.dismissingTimer = Timer.scheduledTimer(timeInterval: Constant.UI.windowInterruptedDismissingInterval, target: self, selector: #selector(self.dismiss), userInfo: nil, repeats: false)
                }
            }
        }
    }
    
    @objc
    func handleTap(tap: UITapGestureRecognizer) {
        dismiss(enableTapAction: true)
    }
    
    @objc
    func handleFloatingWindowPan(pan: UIPanGestureRecognizer) {
        let delta = pan.translation(in: self)
        center = CGPoint(x: center.x + delta.x, y: center.y + delta.y)
        pan.setTranslation(.zero, in: self)
        if pan.state == UIGestureRecognizer.State.ended {
            frame.saveToUserDefault(key: CoreConstant.UserDefaultKey.floatingWindowKey.rawValue)
        }
    }
    
    @objc
    func handleFloatingWindowTap(tap: UITapGestureRecognizer) {
        handleMinimize()
    }
    
    @objc func handleMinimize() {
        guard scene == .floating else { return }
        isMinimized = !isMinimized
        if isMinimized {
            floatingBlockView.isHidden = false
        }
        UIView.animate(withDuration: Constant.UI.animationDuration) { [weak self] in
            guard let self = self else { return }
            self.floatingBlockView.alpha = self.isMinimized ? 1 : 0
            if self.isMinimized {
                self.frame = self.scene.frame
                self.layer.cornerRadius = self.frame.height / 2
            } else {
                self.frame = UIScreen.main.bounds
                self.layer.cornerRadius = 0
            }
        } completion: { [weak self] isFinished in
            guard let self = self else { return }
            self.floatingBlockView.isHidden = !self.isMinimized
        }
    }
    
    func addFloatingWindowBlocker() {
        floatingBlockView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(floatingBlockView)
        floatingBlockView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        floatingBlockView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        floatingBlockView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        floatingBlockView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
    }
}

public class MLWindowManager {
    public static let shared = MLWindowManager()
    
    public var appWindow = UIApplication.shared.windows.first
    public var toastWindow = MLWindow(scene: .toast)
    public var networkStatusWindow = MLWindow(scene: .networkStatus)
    public var audioPlayerWindow = MLWindow(scene: .audioPlayer)
    public var newsWindow = MLWindow(scene: .news)
    public var registrationWindow = MLWindow(scene: .resgistrationBlocker)
    public var alerWindow = MLWindow(scene: .alert)
    public var feedbackWindow = MLWindow(scene: .feedback)
    public var passcodeWindow = MLWindow(scene: .passcode)
    public var floatingWindow = MLWindow(scene: .floating)
    public var openningWindow = MLWindow(scene: .openning)
    public var datePickerWindow = MLWindow(scene: .datePicker)
    public var pickerWindow = MLWindow(scene: .picker)
    public var residencePickerWindow = MLWindow(scene: .residencePicker)
    
        
    static var currentNotificationWindowLevel = MLWindowScene.notification.windowLevel
    
    var notificationWindows = [String: MLWindow]()
    
    let locker = NSLock()
    
    public func promptWindow(scene: MLWindowScene, rootViewController: UIViewController, presentingStyle: MLWindowPresentingStyle, completion: @escaping ((UIViewController?) -> Void)) {
        DispatchQueue.main.asyncExecuteOnMainQueue {
            guard let window = scene.window as? MLWindow else {
                print("Fail to present window for scene: \(scene)")
                return }
            window.presentingStyle = presentingStyle
            window.rootViewController = rootViewController
            window.show {
                completion(window.rootViewController)
            }
        }
    }
    
    public func promptNotificationWindow(rootViewController: UIViewController, tapAction: (()->Void)? = nil, completion: @escaping ((UIViewController?) -> Void)) {
        DispatchQueue.main.asyncExecuteOnMainQueue { [weak self] in
            guard let self = self else { return }
            let window = self.getNotificationWindow(tapAction: tapAction)
            window.presentingStyle = .downward
            window.rootViewController = rootViewController
            window.show {
                completion(window.rootViewController)
            }
        }
    }
    
    public func dismissWindow(scene: MLWindowScene, dismissingStyle: MLWindowDismissingStyle? = nil) {
        DispatchQueue.main.asyncExecuteOnMainQueue {
            guard let window = scene.window as? MLWindow else {
                print("Fail to present window for scene: \(scene)")
                return }
            if let dismissingStyle = dismissingStyle {
                window.dismissingStyle = dismissingStyle
            }
            window.dismiss()
        }
    }
    
    public func getNotificationWindow(tapAction: (()->Void)? = nil) -> MLWindow {
        let window = MLWindow(scene: .notification, tapAction: tapAction)
        MLWindowManager.shared.notificationWindows[window.windowId] = window
        locker.lock()
        MLWindowManager.currentNotificationWindowLevel += 1
        locker.unlock()
        return window
    }
}

